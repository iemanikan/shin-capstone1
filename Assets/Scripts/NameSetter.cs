﻿using UnityEngine;
using Fungus;
using System.Collections;

public class NameSetter : MonoBehaviour {

	public Flowchart MainFlowchart;
	public Flowchart StatManager;

	// Use this for initialization
	void Start () {
	
	}
	
	public void SetName() {
		MainFlowchart.SetStringVariable ("Name", StatManager.GetStringVariable ("Name"));
	}
}

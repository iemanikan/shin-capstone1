﻿using UnityEngine;
using UnityEngine.UI;
using Fungus;
using System.Collections;

public class AgeDisplay : MonoBehaviour {

	public Flowchart MyFlowchart;
	public Text MyText;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		int age = MyFlowchart.GetIntegerVariable ("Age") + 6;
		MyText.text = "Age: " + age.ToString ();
	}
}

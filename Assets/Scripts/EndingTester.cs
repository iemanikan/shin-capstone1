﻿using UnityEngine;
using Fungus;
using System.Collections;

public class EndingTester : MonoBehaviour {

	public Flowchart MyFlowchart;

	public int AthScore;
	public int AcaScore;
	public int ArtScore;
	public int AdventureScore;
	public int SchoolrepScore;
	public int CommunityScore;
	public int FamilyScore;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		AthScore = MyFlowchart.GetIntegerVariable ("AthScore");
		AcaScore = MyFlowchart.GetIntegerVariable ("AcaScore");
		ArtScore = MyFlowchart.GetIntegerVariable ("ArtScore");
		AdventureScore = MyFlowchart.GetIntegerVariable ("Adventure");
		SchoolrepScore = MyFlowchart.GetIntegerVariable ("School");
		CommunityScore = MyFlowchart.GetIntegerVariable ("Community");
		FamilyScore = MyFlowchart.GetIntegerVariable ("Family");
	}
}

﻿using UnityEngine;
using System.Collections;

public class TooltipHandler : MonoBehaviour {

	public GameObject HealthTip;
	public GameObject IntTip;
	public GameObject ConTip;
	public GameObject AthTip;
	public GameObject AcaTip;
	public GameObject ArtTip;

	void Start () {
	
	}
		
	public void DisplayTip(string name) {
		if (name == "HealthTip") {
			HealthTip.SetActive (true);
		} else if (name == "IntTip") {
			IntTip.SetActive (true);
		} else if (name == "ConTip") {
			ConTip.SetActive (true);
		} else if (name == "AthTip") {
			AthTip.SetActive (true);
		} else if (name == "AcaTip") {
			AcaTip.SetActive (true);
		} else if (name == "ArtTip") {
			ArtTip.SetActive (true);
		}
	}

	public void HideTips() {
		HealthTip.SetActive (false);
		IntTip.SetActive (false);
		ConTip.SetActive (false);
		AthTip.SetActive (false);
		AcaTip.SetActive (false);
		ArtTip.SetActive (false);
	}
}

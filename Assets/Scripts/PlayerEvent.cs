﻿using UnityEngine;
using Fungus;
using System.Collections;

public class PlayerEvent : MonoBehaviour {

	public Flowchart MyFlowchart;
	private string blockName;
	public bool hasBeenChosen;
	//public int RequiredAge;

	void Start () {
		blockName = name;
	}

	public void PlayEvent() {
		MyFlowchart.ExecuteBlock (blockName);
	}
}

﻿using UnityEngine;
using Fungus;
using System.Collections;

public class TotalStat : MonoBehaviour {

    public Flowchart StatManager;

    //FINISHED PRODUCT
    public int Health;
    public int Intelligence;
    public int Confidence;
    //BASE STUFF
    public int Phys;
    public int Acad;
    public int Art;
    //IV STUFF
    public int PhysIV;
    public int AcadIV;
    public int ArtIV;
    //EV STUFF
    public int PhysEV;
    public int AcadEV;
    public int ArtEV;
    //ETC
    public int Age;


    public void CalculateHealth  ()
    {
        attainstats();
        //Health = (2 * Phys + PhysIV + PhysEV) * Age /100 + 5;
        Health = Mathf.FloorToInt(((2 * Phys + PhysIV + PhysEV) * Age / 50) + 5);
        StatManager.SetIntegerVariable("Health", Health);
    }
    public void CalculateIntelligence ()
    {
        attainstats();
        //Intelligence = (2 * Acad + AcadIV + AcadEV) * Age / 100 + 5;
        Intelligence = Mathf.FloorToInt(((2 * Acad + AcadIV + AcadEV) * Age / 50) + 5);
        StatManager.SetIntegerVariable("Intelligence", Intelligence);
    }
    public void CalculateConfidence ()
    {
        attainstats();
        //Confidence = (2 * Art + ArtIV + ArtEV) * Age / 100 + 5;
        Confidence = Mathf.FloorToInt(((2 * Art + ArtIV + ArtEV) * Age / 50) + 5);
        StatManager.SetIntegerVariable("Confidence", Confidence);
    }

    void attainstats ()
    {
        //Base
        Phys = StatManager.GetIntegerVariable("Ath");
        Acad = StatManager.GetIntegerVariable("Aca");
        Art = StatManager.GetIntegerVariable("Art");
        //IV
        PhysIV = StatManager.GetIntegerVariable("AthIV");
        AcadIV = StatManager.GetIntegerVariable("AcaIV");
        ArtIV = StatManager.GetIntegerVariable("ArtIV");
        //EV
        PhysEV = StatManager.GetIntegerVariable("AthEV");
        AcadEV = StatManager.GetIntegerVariable("AcaEV");
        ArtEV = StatManager.GetIntegerVariable("ArtEV");
        //MISC
        Age = StatManager.GetIntegerVariable("Age");
        
    }

    // Use this for initialization
    void Start () {
    }
	
	// Update is called once per frame
	void Update () {
	}
}

﻿using UnityEngine;
using Fungus;
using System.Collections;
using System.Collections.Generic;

public class PlayerEventHandler : MonoBehaviour {

	public Flowchart MyFlowchart;
    public Flowchart Statget;
    public GameObject Detector;
	public List<GameObject> CurrentEvents = new List<GameObject>();
	public List<GameObject> eventPool = new List<GameObject>();
	public List<GameObject> PhysPool = new List<GameObject>();
	public List<GameObject> AcadPool = new List<GameObject>();
	public List<GameObject> ArtPool = new List<GameObject>();
	public int EventQuantity;
	private int seedIterator;

	public string MyEventName;
	public string MyPhysEventName;
	public string MyAcadEventName;
	public string MyArtEventName;

	public int PhysWeight;
	public int AcadWeight;
	public int ArtWeight;
	public int PlayerAge;

	public bool isDone;

	private GameObject EventToAdd;
	private bool IsSeeded;
	private bool CanBirthday;
	private bool IsBirthdayTime;

	private int oldPhysWeight;
	private int oldAcadWeight;
	private int oldArtWeight;
	private int RandomWeight;

    void Update () {
        ///get dem variables at yo

        PhysWeight = Statget.GetIntegerVariable("Ath");
        AcadWeight = Statget.GetIntegerVariable("Aca");
        ArtWeight = Statget.GetIntegerVariable("Art");
		PlayerAge = Statget.GetIntegerVariable ("Age");
		RandomWeight = 100 - (PhysWeight + AcadWeight + ArtWeight);

//		if ((oldPhysWeight != PhysWeight) || (oldAcadWeight != AcadWeight) || (oldArtWeight != ArtWeight)) {
//			Rebalance ();
//		}

        if (!IsBirthdayTime) {
			//Get events if we haven't yet.
			if (seedIterator < EventQuantity) {                   
				GetEvents ();
				//Moved seedIterator to the GetEvents function since there are now times when it cannot get an event
				//ie, already been chosen or too young
			} else if (seedIterator == EventQuantity) {
				//Signal that grabbing events is done.
				isDone = true;  
            }
 
			
			if ((!MyFlowchart.GetBooleanVariable ("isPlaying")) && (isDone) && (MyFlowchart.GetIntegerVariable ("EventIterator") < EventQuantity)) {
				MyFlowchart.SetBooleanVariable ("isPlaying", true);
				CurrentEvents [MyFlowchart.GetIntegerVariable ("EventIterator")].gameObject.GetComponent<PlayerEvent> ().PlayEvent ();
            }

			if (MyFlowchart.GetIntegerVariable ("EventIterator") >= EventQuantity) {
				CanBirthday = true;
			}
		}
	}

	public void Rebalance() {
		int total = PhysWeight + AcadWeight + ArtWeight + RandomWeight;
		int newPhysWeight = Mathf.RoundToInt (PhysWeight / total);
		int newAcadWeight = Mathf.RoundToInt (AcadWeight / total);
		int newArtWeight = Mathf.RoundToInt (ArtWeight / total);
		int newRandomWeight = 100 - (newPhysWeight + newAcadWeight + newArtWeight);
		int newTotal = newPhysWeight + newAcadWeight + newArtWeight + newRandomWeight;

		if (newTotal < 100) {
			newRandomWeight += 100 - newTotal;
			Debug.Log ("Was less than 100, adding to random");
		} else if (newTotal == 100) {
			Debug.Log ("Perfect 100");
		} else {
			Debug.Log ("Over 100!");
		}

		Statget.SetIntegerVariable ("Ath", newPhysWeight);
		Statget.SetIntegerVariable("Aca", newAcadWeight);
		Statget.SetIntegerVariable("Art", newArtWeight);
		RandomWeight = newRandomWeight;
	}

	public void HappyBirthday() {
		IsBirthdayTime = true;
		MyFlowchart.SetBooleanVariable ("isPlaying", true);
		MyFlowchart.ExecuteBlock ("Start");       
    }

	void GetEvents() {
		if (!IsSeeded) {
			eventPool.AddRange (GameObject.FindGameObjectsWithTag (MyEventName));
			PhysPool.AddRange (GameObject.FindGameObjectsWithTag (MyPhysEventName));
			AcadPool.AddRange (GameObject.FindGameObjectsWithTag (MyAcadEventName));
			ArtPool.AddRange (GameObject.FindGameObjectsWithTag (MyArtEventName));
		}
		IsSeeded = true;

		int FullTotal = PhysWeight + AcadWeight + ArtWeight;
		Debug.Log ("Max roll is " + FullTotal);
		int DiceRoll = (Random.Range(1, PhysWeight+AcadWeight+ArtWeight));
		//Debug.Log ("I rolled a " + DiceRoll);
		if ((DiceRoll >= 1) && (DiceRoll <= PhysWeight)) {
			int Roll2 = Random.Range (0, PhysPool.Count);
			if (PhysPool [Roll2].gameObject.GetComponent<PlayerEvent> ().hasBeenChosen == false) {
				PhysPool [Roll2].gameObject.GetComponent<PlayerEvent> ().hasBeenChosen = true;
				EventToAdd = PhysPool [Roll2];
				CurrentEvents.Add (EventToAdd);
				PhysPool.Remove (PhysPool [Roll2]);
				seedIterator += 1;
			}
		} else if ((DiceRoll >= PhysWeight + 1) && (DiceRoll <= PhysWeight + AcadWeight)) {
			int Roll2 = Random.Range (0, AcadPool.Count);
			if (AcadPool [Roll2].gameObject.GetComponent<PlayerEvent> ().hasBeenChosen == false) {
				AcadPool [Roll2].gameObject.GetComponent<PlayerEvent> ().hasBeenChosen = true;
				EventToAdd = AcadPool [Roll2];
				CurrentEvents.Add (EventToAdd);
				AcadPool.Remove (AcadPool [Roll2]);
				seedIterator += 1;
			}
		} else if ((DiceRoll >= PhysWeight + AcadWeight + 1) && (DiceRoll <= PhysWeight + AcadWeight + ArtWeight)) {
			int Roll2 = Random.Range (0, ArtPool.Count);
			if (ArtPool [Roll2].gameObject.GetComponent<PlayerEvent> ().hasBeenChosen == false) {
				ArtPool [Roll2].gameObject.GetComponent<PlayerEvent> ().hasBeenChosen = true;
				EventToAdd = ArtPool [Roll2];
				CurrentEvents.Add (EventToAdd);
				ArtPool.Remove (ArtPool [Roll2]);
				seedIterator += 1;
			}
		} else if (DiceRoll > PhysWeight + AcadWeight + ArtWeight) {
			int Roll2 = Random.Range (0, eventPool.Count);
			if (eventPool [Roll2].gameObject.GetComponent<PlayerEvent> ().hasBeenChosen == false) {
				eventPool [Roll2].gameObject.GetComponent<PlayerEvent> ().hasBeenChosen = true;
				EventToAdd = eventPool [Roll2];
				CurrentEvents.Add (EventToAdd);
				eventPool.Remove (eventPool [Roll2]);
				seedIterator += 1;
			}
		}
	}

	public void ClearData() {
		CurrentEvents.Clear ();
		eventPool.Clear ();
		PhysPool.Clear ();
		AcadPool.Clear ();
		ArtPool.Clear ();
		seedIterator = 0;
		IsBirthdayTime = false;
		CanBirthday = false;
		IsSeeded = false;
		isDone = false;
		MyFlowchart.SetIntegerVariable ("EventIterator", 0);
		MyFlowchart.SetBooleanVariable ("isPlaying", false);
	}

	public void LockVars() {
		oldPhysWeight = PhysWeight;
		oldAcadWeight = AcadWeight;
		oldArtWeight = ArtWeight;
	}
}
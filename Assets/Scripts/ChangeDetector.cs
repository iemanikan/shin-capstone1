﻿using UnityEngine;
using Fungus;
using System.Collections;

public class ChangeDetector : MonoBehaviour
{
	public Flowchart StatMaster;

	private int baseHealth;
	private int baseInt;
	private int baseCon;

	private int baseAth;
	private int baseAca;
	private int baseArt;

	public GameObject HealthUp;
	public GameObject HealthDown;

	public GameObject IntUp;
	public GameObject IntDown;

	public GameObject ConUp;
	public GameObject ConDown;

	public GameObject AthUp;
	public GameObject AthDown;

	public GameObject AcadUp;
	public GameObject AcadDown;

	public GameObject ArtUp;
	public GameObject ArtDown;

	private bool startTracking;

	private int newTotal;
	private int newAca;
	private int newAth;
	private int newArt;
	private int newInterestSum;
	private int remainder;

	//When stats are first initialized, RUN ME
	public void Initialize ()
	{
		baseHealth = StatMaster.GetIntegerVariable ("Health");
		baseInt = StatMaster.GetIntegerVariable ("Intelligence");
		baseCon = StatMaster.GetIntegerVariable ("Confidence");

		baseAth = StatMaster.GetIntegerVariable ("Ath");
		baseAca = StatMaster.GetIntegerVariable ("Aca");
		baseArt = StatMaster.GetIntegerVariable ("Art");

		startTracking = true;
	}

	//--> You can leave it in update or just have a function call after each event?
	//--> If it doesn't make it laggy as fuck I think it can work in update

	void Update ()
	{
		//0 catchers
		if (StatMaster.GetIntegerVariable ("Health") <= 0) {
			StatMaster.SetIntegerVariable ("Health", 1);
		}

		if (StatMaster.GetIntegerVariable ("Intelligence") <= 0) {
			StatMaster.SetIntegerVariable ("Intelligence", 1);
		}

		if (StatMaster.GetIntegerVariable ("Confidence") <= 0) {
			StatMaster.SetIntegerVariable ("Confidence", 1);
		}

		//Over 100 catchers
//		if (StatMaster.GetIntegerVariable ("Ath") + StatMaster.GetIntegerVariable ("Aca") +
//		    StatMaster.GetIntegerVariable ("Art") > 100) {
//			startTracking = false;
//			newTotal = StatMaster.GetIntegerVariable ("Ath") + StatMaster.GetIntegerVariable ("Aca") +
//			             StatMaster.GetIntegerVariable ("Art");
//			Debug.Log (newTotal);
//
//			newAth = Mathf.RoundToInt(StatMaster.GetIntegerVariable ("Ath") / newTotal);
//			Debug.Log ("New Ath: " + newAth);
//			newAca = Mathf.RoundToInt(StatMaster.GetIntegerVariable ("Aca") / newTotal);
//			Debug.Log ("New Aca: " + newAca);
//			newArt = Mathf.RoundToInt(StatMaster.GetIntegerVariable ("Art") / newTotal);
//			Debug.Log ("New Art: " + newArt);
//			newInterestSum = newAth + newAca + newArt;
//
//			if (newInterestSum < 100) {
//				remainder = 100 - newInterestSum;
//				Debug.Log (remainder + " points over 100");
//				for (int i = 0; i < remainder; i++) {
//					int DiceRoll = (Random.Range(1, 3));
//					if (DiceRoll == 1) {
//						newAth++;
//					} else if (DiceRoll == 2) {
//						newAca++;
//					} else if (DiceRoll == 3) {
//						newArt++;
//					}
//				}
//			} else if (newInterestSum > 100) {
//				remainder = newInterestSum - 100;
//				Debug.Log (remainder + " points under 100");
//				for (int i = 0; i < remainder; i++) {
//					int DiceRoll = (Random.Range(1, 3));
//					if (DiceRoll == 1) {
//						newAth--;
//					} else if (DiceRoll == 2) {
//						newAca--;
//					} else if (DiceRoll == 3) {
//						newArt--;
//					}
//				}
//			}
//
//			StatMaster.SetIntegerVariable ("Ath", newAth);
//			StatMaster.SetIntegerVariable ("Aca", newAca);
//			StatMaster.SetIntegerVariable ("Art", newArt);
//			startTracking = true;
//		}

		if (startTracking) {
			//If my baseHealth != what the stats say it is, ie it changed...
			if (baseHealth != StatMaster.GetIntegerVariable ("Health")) {
				if (baseHealth < StatMaster.GetIntegerVariable ("Health")) {
					HealthUp.SetActive (true);
					baseHealth = StatMaster.GetIntegerVariable ("Health");
				} else {
					HealthDown.SetActive (true);
					baseHealth = StatMaster.GetIntegerVariable ("Health");
				}
			}
			
			if (baseInt != StatMaster.GetIntegerVariable ("Intelligence")) {
				if (baseInt < StatMaster.GetIntegerVariable ("Intelligence")) {
					IntUp.SetActive (true);
					baseInt = StatMaster.GetIntegerVariable ("Intelligence");
				} else {
					IntDown.SetActive (true);
					baseInt = StatMaster.GetIntegerVariable ("Intelligence");
				}
			}

			if (baseCon != StatMaster.GetIntegerVariable ("Confidence")) {
				if (baseCon < StatMaster.GetIntegerVariable ("Confidence")) {
					ConUp.SetActive (true);
					baseCon = StatMaster.GetIntegerVariable ("Confidence");
				} else {
					ConDown.SetActive (true);
					baseCon = StatMaster.GetIntegerVariable ("Confidence");
				}
			}

			if (baseAth != StatMaster.GetIntegerVariable ("Ath")) {
				if (baseAth < StatMaster.GetIntegerVariable ("Ath")) {
					AthUp.SetActive (true);
					baseAth = StatMaster.GetIntegerVariable ("Ath");
				} else {
					AthDown.SetActive (true);
					baseAth = StatMaster.GetIntegerVariable ("Ath");
				}
			}

			if (baseAca != StatMaster.GetIntegerVariable ("Aca")) {
				if (baseAca < StatMaster.GetIntegerVariable ("Aca")) {
					AcadUp.SetActive (true);
					baseAca = StatMaster.GetIntegerVariable ("Aca");
				} else {
					AcadDown.SetActive (true);
					baseAca = StatMaster.GetIntegerVariable ("Aca");
				}
			}

			if (baseArt != StatMaster.GetIntegerVariable ("Art")) {
				if (baseArt < StatMaster.GetIntegerVariable ("Art")) {
					ArtUp.SetActive (true);
					baseArt = StatMaster.GetIntegerVariable ("Art");
				} else {
					ArtDown.SetActive (true);
					baseArt = StatMaster.GetIntegerVariable ("Art");
				}
			}
		}
	}
}
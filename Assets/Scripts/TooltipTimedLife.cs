﻿using UnityEngine;
using System.Collections;

public class TooltipTimedLife : MonoBehaviour {

	public float Duration;

	// Use this for initialization
	void Start () {
		Duration = 2.0f;
	}
	
	// Update is called once per frame
	void Update () {
		Duration -= 1 * Time.deltaTime;
		if (Duration <= 0) {
			Duration = 2.0f;
			gameObject.SetActive (false);
		}
	}

	public void Refresh() {
		Duration = 2.0f;
	}
}
